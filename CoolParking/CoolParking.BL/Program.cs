using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {
            TimerService withdrawTimer = new TimerService(Settings.parkingPaymentTime);
            TimerService timerService = new TimerService(Settings.parkingLogTime);
            LogService logTimer = new LogService();
            
            ParkingService ps = new ParkingService(withdrawTimer, timerService,  logTimer);

            withdrawTimer.Elapsed += ps.WithdrawHandler;
            timerService.Elapsed += logTimer.WriteHandler;

            Console.WriteLine("***Welcome to Cool Parking***");
            
            while (true)
            {
                Console.WriteLine("Menu");
                Console.WriteLine("*", 20);
                Console.WriteLine("1. Show current balance of the parking.");
                Console.WriteLine("2. Show last earned money.");
                Console.WriteLine("3. Show status about park places.");
                Console.WriteLine("4. Show last transactions.");
                Console.WriteLine("5. Show history of transactions.");
                Console.WriteLine("6. Show vehicles on parking.");
                Console.WriteLine("7. Set vehicle on parking.");
                Console.WriteLine("8. Take vehicle from parking.");
                Console.WriteLine("9. Top-up valance of vehicle.");
                Console.WriteLine("q. Quit.");
                
                char key = Console.ReadKey().KeyChar;
                
                switch (key)
                {
                    case '1':
                    {
                        Console.WriteLine($"Current balance of parking: {ps.GetBalance()}");
                        continue;
                    }
                    
                    case '2':
                    {
                        
                        continue;
                    }
                    
                    case '3':
                    {
                        Console.WriteLine($"Parkplaces: {ps.GetFreePlaces()} / {ps.GetCapacity()} are free");
                        continue;
                    }
                    
                    case '4':
                    {
                        foreach (TransactionInfo t in ps.GetLastParkingTransactions())
                        {
                            Console.WriteLine(t.GetTransactionAsString());
                        }
                        continue;
                    }
                    
                    case '5':
                    {
                        logTimer.Read();
                        continue;
                    }
                    
                    case '6':
                    {
                        Console.WriteLine("Vehicles on parking:");
                        Console.WriteLine("*", 20);
                        
                        foreach (Vehicle vehicle in ps.GetVehicles())
                        {
                            Console.WriteLine($"{vehicle.Id}   {vehicle.VehicleType}");
                        }
                        
                        continue;
                    }
                    
                    case '7':
                    {
                        Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
                        string id;
                        VehicleType vehicleType;
                        decimal convertedAmount;
                        
                        while (true)
                        {
                            Console.WriteLine("Please, input ID of your vehicle:");
                            id = Console.ReadLine();

                            if (pattern.IsMatch(id))
                            {
                                break;
                            }
                            
                            Console.WriteLine("Id is not valid! Try again!");
                        }
                        
                        while (true)
                        {
                            Console.WriteLine("Select type of your vehicle:");
                            Console.WriteLine("1. PassengerCar");
                            Console.WriteLine("2. Truck");
                            Console.WriteLine("3. Bus");
                            Console.WriteLine("4. Motorcycle");

                            char caseKey = Console.ReadKey().KeyChar;

                            switch (caseKey)
                            {
                                case '1':
                                {
                                    vehicleType = VehicleType.PassengerCar;
                                    break;
                                }
                                
                                case '2':
                                {
                                    vehicleType = VehicleType.Truck;
                                    break;
                                }
                                
                                case '3':
                                {
                                    vehicleType = VehicleType.Bus;
                                    break;
                                }
                                
                                case '4':
                                {
                                    vehicleType = VehicleType.Motorcycle;
                                    break;
                                }

                                default:
                                {
                                    Console.WriteLine("Invalid type! Try again!");
                                    continue;
                                }
                            }
                            
                            break;
                        }
                        
                        while (true)
                        {
                            Console.WriteLine("Please, input amount of money:");
                            string amount = Console.ReadLine();

                            try
                            {
                                convertedAmount = Convert.ToDecimal(amount);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Invalid format! Please, use only numbers!");
                                continue;
                            }

                            break;
                        }

                        Vehicle vehicle = new Vehicle(id, vehicleType, convertedAmount);
                        ps.AddVehicle(vehicle);
                        
                        continue;
                    }
                    
                    case '8':
                    {
                        Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
                        string id;

                        while (true)
                        {
                            Console.WriteLine("Please, input ID of your vehicle:");
                            id = Console.ReadLine();

                            if (pattern.IsMatch(id))
                            {
                                break;
                            }
                            
                            Console.WriteLine("Id is not valid! Try again!");
                        }
                        
                        ps.RemoveVehicle(id);
                        continue;
                    }
                    
                    case '9':
                    {
                        Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
                        string id;
                        decimal convertedAmount;

                        while (true)
                        {
                            Console.WriteLine("Please, input ID of your vehicle:");
                            id = Console.ReadLine();

                            if (pattern.IsMatch(id))
                            {
                                break;
                            }
                            
                            Console.WriteLine("Id is not valid! Try again!");
                        }

                        while (true)
                        {
                            Console.WriteLine("Please, input amount of money:");
                            string amount = Console.ReadLine();

                            try
                            {
                                convertedAmount = Convert.ToDecimal(amount);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Invalid format! Please, use only numbers!");
                                continue;
                            }

                            break;
                        }

                        ps.TopUpVehicle(id, convertedAmount);
                        continue;
                    }
                }

                switch (key)
                {
                    case 'q':
                    {
                        Console.WriteLine("Bye!");
                        break;
                    }
                }
                
                break;
            }
        }
    }
}