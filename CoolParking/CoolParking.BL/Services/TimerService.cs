﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        private Timer timer;

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            Interval = interval;
            timer = new Timer(Interval);
            timer.AutoReset = true;
        }

        public void Start()
        {
            timer.Elapsed += Elapsed;
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Enabled = false;
        }

        public void Dispose()
        {
            timer.Dispose();
        }
    }
}