﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking _parking = Parking.GetParking();
        private TimerService _withdrawTimer;
        private TimerService _timerService;
        private LogService _logTimer;
        public List<TransactionInfo> _transactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService timerService, ILogService logTimer)
        {
            _withdrawTimer = withdrawTimer as TimerService;
            _timerService = timerService as TimerService;
            _logTimer = logTimer as LogService;
            _withdrawTimer.Start();
            _timerService.Start();
        }
        
        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingInitialCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.parkingInitialCapacity - _parking._listOfVehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            
            foreach (KeyValuePair<string, Vehicle> item in _parking._listOfVehicles)
            {
                list.Add(item.Value);
            }

            ReadOnlyCollection<Vehicle> readOnlyCollection = list.AsReadOnly();

            return readOnlyCollection;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (!_parking._listOfVehicles.TryAdd(vehicle.Id, vehicle))
            {
                throw new InvalidOperationException();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = _parking._listOfVehicles[vehicleId];
            if (vehicle.Balance > 0)
            {
                _parking._listOfVehicles.Remove(vehicleId);
            }

            else
            {
                throw new InvalidOperationException();
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = _parking._listOfVehicles[vehicleId];
            vehicle.Balance += sum;
        }

        public void WithdrawHandler(object sender, ElapsedEventArgs e)
        {
            Withdraw();
        }
        
        public void Withdraw()
        {
            foreach (KeyValuePair<string, Vehicle> item in _parking._listOfVehicles)
            {
                Vehicle vehicle = item.Value;
                VehicleType vehicleType = vehicle.VehicleType;
                decimal tax;
                decimal difference;

                if (vehicleType is VehicleType.PassengerCar)
                {
                    tax = Settings.passengerCarTax;
                }
                
                else if (vehicleType is VehicleType.Truck)
                {
                    tax = Settings.truckTax;
                }
                
                else if (vehicleType is VehicleType.Bus)
                {
                    tax = Settings.busTax;
                }

                else
                {
                    tax = Settings.motorcycleTax;
                }

                if (vehicle.Balance >= tax)
                {
                    vehicle.Balance -= tax;
                    _parking.Balance += tax;
                }
                else
                {
                    difference = tax - vehicle.Balance;
                    vehicle.Balance = (0 - difference) * Settings.fine;
                    _parking.Balance += difference * Settings.fine;
                }

                TransactionInfo transactionInfo = new TransactionInfo(vehicle.Id, tax);
                _logTimer.Transaction = transactionInfo;
                _transactions.Add(transactionInfo);
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return this._logTimer.Read();
        }

        public void Dispose()
        {
            _parking._listOfVehicles.Clear();
        }
    }
}