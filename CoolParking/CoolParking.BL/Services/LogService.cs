﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        
        public TransactionInfo Transaction { get; set; }

        public LogService()
        {
            LogPath = Settings.logFile;
        }
        
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        
        public void WriteHandler(object sender, ElapsedEventArgs e)
        {
            WriteTr();
            
        }
        
        public void WriteTr()
        {
            Write(Transaction.GetTransactionAsString());
        }
        
        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath))
            {
                sw.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                using (StreamReader sr = new StreamReader(LogPath))
                {
                    string logLine;
                    while ((logLine = sr.ReadLine()) != null)
                    {
                        sb.Append(logLine);
                    }
                }
                
                return sb.ToString();
            }
            catch (FileNotFoundException e)
            {
                throw new InvalidOperationException();
            }
        }
    }
}