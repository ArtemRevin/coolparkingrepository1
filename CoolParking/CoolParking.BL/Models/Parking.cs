﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        public decimal Balance { get; set; }

        public Dictionary<string, Vehicle> _listOfVehicles;

        public static Parking _parking;
        
        private Parking()
        {
            Balance = Settings.parkingInitialBalance;
            _listOfVehicles = new Dictionary<string, Vehicle>(Settings.parkingInitialCapacity);
        }
        
        public static Parking GetParking()
        {
            if (_parking == null)
            {
                _parking = new Parking();
                return _parking;
            }

            return _parking;
        }
    }
}