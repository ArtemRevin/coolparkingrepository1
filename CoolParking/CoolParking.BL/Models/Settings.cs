﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal parkingInitialBalance = 0;
        public static int parkingInitialCapacity = 10;
        public static int parkingPaymentTime = 5000;
        public static int parkingLogTime = 60000;
        public static decimal passengerCarTax = 2;
        public static decimal truckTax = 5;
        public static decimal busTax = 3.5m;
        public static decimal motorcycleTax = 1;
        public static decimal fine = 2.5m;
        public static string logFile = @"Logs/Log.txt";
    }
}