using System.Collections;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        //GET api/parking/balance
        [HttpGet("{balance}")]
        public ActionResult<decimal> Balance()
        {
            return Parking.GetParking().Balance;
        }
        
        //GET api/parking/capacity
        [HttpGet("{capacity}")]
        public ActionResult<int> Capacity()
        {
            return Settings.parkingInitialCapacity;
        }
        
        //GET api/parking/freePlaces
        [HttpGet("{freeplaces}")]
        public ActionResult<int> Freeplaces()
        {
            return Settings.parkingInitialCapacity - Parking.GetParking()._listOfVehicles.Count;
        }
    }
}